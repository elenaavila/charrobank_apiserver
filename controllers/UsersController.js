const utils = require('../utils/Utils');
const crypt = require('../utils/Crypt');

const userSrv = require('../services/UsersService');
const accountSrv = require('../services/AccountsService');

function enrollmentV1(req, res){
    console.log("POST /charrobank/v1/enrollment");
    var newUser = req.body;
    //console.log("enrollmentV1.- req.body", req.body);
    //console.log("enrollmentV1.- JSON.stringify({req.body})", JSON.stringify(newUser));

    // Validar que vienen los campos necesarios
    if (!utils.validateUserFields(newUser)) {
       return res.status(400).send({"mensaje" : "Campos obligatorios no informados"});
    }
    //console.log("El email del usuario es "+req.body.email);
    //Compruebo que no exista ya un usuario con ese email
    userSrv.findUserByEmail(req.body.email, function (err, user) {
       if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
       //Usuario ya existe
       if (user) return res.status(422).send({"mensaje" : "El email utilizado ya esta registrado."});

       //console.log("enrollmentV1.- newUser", newUser);
       newUser.password = crypt.hash(req.body.password);
       //console.log("enrollmentV1.- newUser despues de hash", newUser);

       var newUserParseado = JSON.stringify(newUser);
       //console.log("enrollmentV1.- newUserParseado", newUserParseado);
       newUser.logged=true;
       //Creamos el nuevo usuario
       userSrv.createUser(newUser, function (err, user) {
        if (err || !user) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
        console.log('Usuario creado OK');
        //Si se ha dado de alta, generamos token (login)
        var token = crypt.generateToken(user._id);
        var userCreated = {
          id : user._id,
          email : user.email,
          token : token
        };
        var newAccount = {
            user_id: user._id,
            balance: 0.00,
            currency: "EUR",
            openingDate: new Date(),
            status: "ACTIVA"
        };
        //Creamos la nueva cuenta
        accountSrv.createAccount(newAccount, function (err, account) {
          if (err || !account) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
          console.log('Cuenta nueva creada OK');
          //Añado el token al header
          //res.setHeader('tsec', token);
          return res.status(201).send(userCreated);
        });
      });
    });
}

function getUserByIdV1(req, res){
  console.log("GET getUserByIdV1 /charrobank/v1/users/:id");
  var userId = Number.parseInt(req.params.id);
  var userIdToken = Number.parseInt(req.userIdToken);
  //console.log("getUserByIdV1.- userId url: "+userId+"- userIdToken:"+userIdToken);
  if(userIdToken != userId){
    console.log("getUserByIdV1.- ID USUARIO (req.params.id) NO ES EL MISMO QUE el del TOKEN");
    return res.status(422).send({"mensaje" : "No tiene permisos para realizar la operacion."});
  }

  userSrv.findUserById(userId, function (err, user) {
     if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
     if (!user) return res.status(401).send({"mensaje" : "Usuario no existe"});

     //No incluimos la password
     delete user.password;
     user.id = user._id;
     delete user._id;
     delete user.logged;
     //console.log("USUARIO ENVIADO A FRONT:", user);
     return res.status(200).send(user);
  });
}

module.exports.enrollmentV1 = enrollmentV1;
module.exports.getUserByIdV1 = getUserByIdV1;
