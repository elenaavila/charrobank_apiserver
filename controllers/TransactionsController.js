const utils = require('../utils/Utils');

const accountSrv = require('../services/AccountsService');
const transactionSrv = require('../services/TransactionsService');

function createTransactionV1(req, res){
    console.log("POST /charrobank/v1/accounts/:id/transactions");
    //console.log("createTransactionV1.- req.body", req.body);
    //console.log("createTransactionV1.- JSON.stringify({req.body})", JSON.stringify(req.body));

    // Validar que vienen los campos necesarios para crear un mov nuevo asociado a cuenta
    if (   !req.userIdToken || !req.params.id
        || !utils.validateTransactionFields(req.body)) {
       return res.status(400).send({"mensaje" : "Campos obligatorios no informados"});
    }
    //console.log("createTransactionV1 El id del usuario es "+req.userIdToken);
    //console.log("createTransactionV1 El id de la cuenta es "+req.params.id);

   //Recuperamos la cuenta a la que se asociara el movimiento
   accountSrv.findAccountById(Number.parseInt(req.params.id), function (err, account) {
      if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
      if (!account) return res.status(422).send({"mensaje" : "La cuenta no existe"});

      var userIdToken = Number.parseInt(req.userIdToken);
      var userIdAccount = Number.parseInt(account.user_id);
      if(userIdToken != userIdAccount){
        console.log("createTransactionV1.- USUARIO logado NO ES EL MISMO QUE el de la cuenta. ");
        return res.status(422).send({"mensaje" : "No tiene permisos para realizar la operacion."});
      }
      //console.log("CUENTA RECUPERADA:", account);

      // Se registra el movimiento y se actualiza el balance de la cuenta
       var type;
       var amount;
       var newBalance;
       if (req.body.type === 'REINTEGRO') {
         type = 'Reintegro';
         newBalance = Number.parseFloat(account.balance) - Number.parseFloat(req.body.amount);
         amount = '-' + req.body.amount;
       } else if (req.body.type === 'INGRESO') {//Ingreso
         type = 'Ingreso';
         newBalance = Number.parseFloat(account.balance) + Number.parseFloat(req.body.amount);
         amount = '' + req.body.amount;
       } else {
         return res.status(422).send({"mensaje" : "La operación indicada no es válida."});
       }

       var newTransaction = {
         account_id:req.params.id,
         amount: amount,
         balance: newBalance,
         currency: req.body.currency,
         description: req.body.description,
         type: type,
         date: new Date()
      };
      //Insertamos el nuevo movimiento:
      transactionSrv.createTransaction(newTransaction, function (err, transaction) {
         account.balance = transaction.balance;
         accountSrv.updateAccount(account, function (err, account) {
           if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
           // Si todo ha ido correctamente, devolvemos un 201, mov creado y saldo cuenta actualizado
           return res.status(201).send({"mensaje" : "La operacion se ha realizado correctamente"});
       });
     });
   });
}

function getTransactionsByAccountIdV1(req, res){
  console.log("GET getTransactionsByAccountIdV1 /charrobank/v1/accounts/:id/transactions");
  accountSrv.findAccountById(Number.parseInt(req.params.id), function (err, account) {
     if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
     if (!account) return res.status(422).send({"mensaje" : "No existe la cuenta"});
     //console.log("CUENTA RECUPERADA:", account);
     //Validamos que la cuenta pertence al usuario logado
     var userIdToken = Number.parseInt(req.userIdToken);
     var userIdAccount = Number.parseInt(account.user_id);
     if(userIdToken != userIdAccount){
       console.log("getTransactionsByAccountIdV1.- USUARIO logado NO ES EL MISMO QUE el de la cuenta. ");
       return res.status(422).send({"mensaje" : "No tiene permisos para realizar la operacion."});
     }
    transactionSrv.findTransactionsByAccountId(Number.parseInt(req.params.id), function (err, transactions) {
      if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
      //if (!transactions)return res.status(204).send({"mensaje" : "No hay movimientos para esta cuenta"});

      var movements = {
        iban: account.iban,
        transactions: transactions
      }

      return res.status(200).send(movements);
    });
  });
}

function deleteTransactionsByAccountIdV1(req, res){
  console.log("GET deleteTransactionsByAccountIdV1 /charrobank/v1/accounts/:id/transactions");
  transactionSrv.deleteTransactions(Number.parseInt(req.params.id), function (err, transactions) {
     if (err || !transactions) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
    console.log("MOVIMIENTOS BORRADOS:", transactions);
    return res.status(200).send({"removedTransactions": transactions.removed});

  });
}

module.exports.createTransactionV1 = createTransactionV1;
module.exports.getTransactionsByAccountIdV1 = getTransactionsByAccountIdV1;
module.exports.deleteTransactionsByAccountIdV1 = deleteTransactionsByAccountIdV1;
