const requestJson = require('request-json');

const mLabUrl = process.env.MLAB_API_URL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const mLabUrlBD = process.env.MLAB_API_URL_BD;

const utils = require('../utils/Utils');

function findUserByEmail(email, callback) {
  var httpClient = requestJson.createClient(mLabUrl);
  var query = "q=" + JSON.stringify({"email": email});
  var url = "users?" + query + "&" + mLabAPIKey;
  //console.log("findUserByEmail.- URL: ", url);
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("findUserByEmail.- Error en el acceso a base de datos", err);
        return callback(err);
      } else {
        if (body.length > 0) {
          var user = body[0];
          console.log("findUserByEmail.- Usuario encontrado");
          return callback(err, user);
        } else {
          console.log("findUserByEmail.- Usuario NO encontrado");
          return callback(null, null);
        }
      }
    }
  );
}

function findUserById(userId, callback) {
  var httpClient = requestJson.createClient(mLabUrl);
  var query = "q=" + JSON.stringify({"_id": userId});
  var url = "users?" + query + "&" + mLabAPIKey;
  //console.log("findUserById.- URL: ", url);
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("findUserById.- Error en el acceso a base de datos", err);
        return callback(err);
      } else {
        if (body.length > 0) {
          var user = body[0];
          console.log("findUserById.- Usuario encontrado");
          return callback(err, user);
        } else {
          console.log("findUserById.- Usuario NO encontrado");
          return callback(null, null);
        }
      }
    }
  );
}

function createUser(newUser, callback) {
  utils.nextValSequence("users", function (err, numIndex) {
     if (err) return callback(err);
     if (!numIndex) return callback(err, numIndex);
     //Insertamos el nuevo usuario con el siguiente "id" de la secuencia
     newUser._id = numIndex;
     //console.log("en createUser FIN OK nextValSequence.- newUser:", newUser);
     var httpClient = requestJson.createClient(mLabUrl);
     var url = "users?" + mLabAPIKey;
     //console.log("createUser.- URL: ", url);
     httpClient.post(url, newUser,
       function(err, resMlab, body){
         if(err) return callback(err);
         console.log("createUser.- Usuario creado");
         //console.log("createUser.-body:", body);
         var user = body;
         return callback(err, user);
     });
  });
}

function updateUser(userId, queryPutBody, callback){
  console.log("updateUser");
  var queryId = "q=" + JSON.stringify({"_id": Number.parseInt(userId)});
  //console.log("updateUser.- QUERY ID: " + queryId+ "-queryPutBody:"+queryPutBody);
  //*PUT PARA ACTUALIZAR EL USUARIO CON LOGGED TRUE*
  //var queryPutBody = '{"$set":{"logged":true}}';
  var httpClient = requestJson.createClient(mLabUrl);
  httpClient.put("users?" + queryId + "&" + mLabAPIKey, JSON.parse(queryPutBody),
    function(err, resMlab, body){
      if(err) return callback(err);
      console.log("updateUser.-OK");
      return callback(err, body);
  });
}


module.exports.findUserByEmail = findUserByEmail;
module.exports.findUserById = findUserById;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
