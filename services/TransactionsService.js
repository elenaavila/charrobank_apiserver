const requestJson = require('request-json');

const utils = require('../utils/Utils');

const mLabUrl = process.env.MLAB_API_URL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabUrlBD = process.env.MLAB_API_URL_BD;

function createTransaction(transaction, callback) {
  utils.nextValSequence("transactions", function (err, numIndex) {
     if (err) return callback(err);
     if (!numIndex) return callback(err, numIndex);
     //Insertamos el nuevo mov de la cuenta con el siguiente "id" de la secuencia transactions
     transaction._id = numIndex;
     transaction.account_id = Number.parseInt(transaction.account_id);
     //console.log("createTransaction FIN OK nextValSequence.- transaction:", transaction);
     var httpClient = requestJson.createClient(mLabUrl);
     var url = "transactions?" + mLabAPIKey;
     //console.log("createTransaction.- URL: ", url);
     httpClient.post(url, transaction,
       function(err, resMlab, body){
         console.log("createTransaction.- Movimiento creado");
         if(err) return callback(err);
         //console.log("createTransaction.-body:", body);
         return callback(err, body);
     });
  });
}

function findTransactionsByAccountId(accountId, callback) {
  console.log("findTransactionsByAccountId.- ", accountId);
  var httpClient = requestJson.createClient(mLabUrl);
  var query = "q=" + JSON.stringify({"account_id": Number.parseInt(accountId)});
  var sort = 's={"date": -1}';
  var url = "transactions?" + query + "&" + sort + "&" + mLabAPIKey;
  //console.log("accountId.- URL: ", url);
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("accountId.- Error en el acceso a base de datos", err);
        return callback(err);
      } else {
        if (body.length > 0) {
          console.log("accountId.-  Movimientos encontrados");
          return callback(err, body);
        } else {
          console.log("accountId.- Movimientos no existen para la cuenta", accountId);
          return callback(null, null);
        }
      }
    }
  );
}

function deleteTransactions(accountId, callback){
  var queryId = "q=" + JSON.stringify({"account_id": Number.parseInt(accountId)});
  //console.log("deleteTransactions.- QUERY ID: " + queryId);
  var httpClient = requestJson.createClient(mLabUrl);
  httpClient.put("transactions?" + queryId + "&" + mLabAPIKey, [],
    function(err, resMlab, body){
      if(err) return callback(err);
      console.log("deleteTransactions OK");
      return callback(err, body);
  });
}


module.exports.createTransaction = createTransaction;
module.exports.findTransactionsByAccountId = findTransactionsByAccountId;
module.exports.deleteTransactions = deleteTransactions;
