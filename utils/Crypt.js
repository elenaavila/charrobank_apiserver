const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const jwtSecret = process.env.JWT_SECRET;

function hash(data){
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
  return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

function generateToken(user_id){
  //console.log("generateToken user_id:", user_id);
  //24 horas para expiracion. Devolvemos el token
  return jwt.sign({ id: user_id }, jwtSecret, {expiresIn: 60 * 60 * 24});
}

function checkToken(token){
  console.log("checkToken token:", token);
  jwt.verify(token, jwtSecret, function(err, decoded) {
    //console.log("checkToken decoded:", decoded);
    if(err){
      console.log("checkToken err");
      return null;//Si hay error, devolvemos -1
    }else{
      //console.log("checkToken decoded.id:", decoded.id);
      console.log("checkToken ok");
      // Si el token es correcto, devovlemos el id del usuario logado
      return decoded.id;
    }
  });
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
module.exports.generateToken = generateToken;
module.exports.checkToken = checkToken;
